import os
import choose_options as choose

choose.awake()

while True:
    if "exit" not in choose.moves:
        choose.options1()

        choose1 = input("Type option in **: ")
        os.system('cls')

        if choose1 == "examine":
            choose.examine()

        elif choose1 == "look":
            choose.look()

        elif choose1 == "room2":
            if "Flashlight" not in choose.equipment:
                choose.not_fl()
            else:
                choose.room2()
                if choose.use_or_back == 0:
                    continue


                while True:
                    choose.options2()

                    choose2 = input("Type option in **: ")
                    os.system('cls')

                    if choose2 == "table":
                        choose.table()

                    elif choose2 == "bed":
                        choose.bed()

                    elif choose2 == "wardrobe":
                        choose.wardrobe()

                    elif choose2 == "painting" or choose2 == "box":
                        choose.painting()

                    elif choose2 == "door":
                        choose.door()
                        if "exit" in choose.moves:
                            break

                    elif choose2 == "back":
                        choose.back()
                        if "back" in choose.moves:
                            choose.moves.remove("back")
                            break

                    elif choose2 == "eq":
                        choose.eq()

        elif choose1 == "eq":
            choose.eq()
    else:
        break
choose.end()
