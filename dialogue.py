awake = ("""You open your eyes and find yourself in a completely dark room.""")
room = ("""You awaken in an unfamiliar room, bathed in a delicate glow from a wall-mounted lamp above you. 
At the opposite end of the room, you notice a desk with a computer. 
You're uncertain of how you arrived here or what you're doing, but the need to escape looms over you. 
The soft light from the lamp and the glow of the computer screen cast an enigmatic atmosphere. 
Your only certainty is that you must find a way out.""")
computer = ("""You walk up to the desk, power on the computer, but it remains unresponsive.""")
light = ("""As you explore the room, you notice a flashlight on the table.""")
dark_2room = ("""You hesitate to enter the second room as it appears too dark to navigate safely. 
You might need more light or information before venturing further.""")
room2 = ("""You turn on the flashlight, illuminating the room. 
The light casts warm reflections on the walls, revealing the outlines of furniture and details. 
Despite your thorough search, you don't find any significant clues or items in the first room. 
Determined to continue your search, you head towards the second room.""")
in_room2 = """With the aid of the flashlight, you explore the second room, which is slightly larger than the previous one. 
It appears to be a neatly arranged space, featuring a table in the center, a bed, a tall wardrobe, 
and a slightly tilted painting on the wall. 
Additionally, you notice a set of metal doors at the entrance, adorned with a peculiar screen instead of a traditional handle."""
table = """You walk up to the table, but unfortunately, it is completely empty. There are no clues or items of interest."""
bed = """It looks so ordinary that you would like to take a nap. It's not time for that"""
wardrobe = """You decide to see what the wardrobe might conceal. 
After a brief search among ordinary items, you find a crowbar. It might be useful."""
painting = """You examine the tilted painting, and it seems like there might be something behind it."""
removed_painting = """\nYou examine the tilted painting and discover a small box with fuses hidden behind it. 
You notice one fuse is missing. Could it be somewhere in these rooms?"""
door = """You walk up to the metal doors and attempt to open them, but they remain unmoved. 
A message on a small screen reads "No access" with a button below. 
As you inspect closely, you notice thin cables extending from the doors to the fuse box 
and from the fuse box to the first room."""
back = """As you try to return to the previous room, you step on a loose board in the floor. 
It feels precarious and seems like it could be pried open."""
board = """With the help of the crowbar, you manage to open the loose board in the floor. 
To your surprise, you find the missing fuse hidden beneath it."""
box = """You approach the box hidden behind the removed painting on the wall."""
fuse = """You notice a slot that perfectly fits the missing fuse. You place it into its slot.
Suddenly, the light in the first room unexpectedly comes on."""
unlock_door = """You decide to revisit the computer. 
Upon restarting it, you examine the desktop and notice a file named "door.py". 
Intrigued, you double-click on it, revealing an unfinished code. 
Using your impressive hacking skills, you add a few lines of code and run the program. 
Suddenly, you hear a strange *beep* from the second room."""
exit1 = """You approach the metal doors once more, and this time, a message appears on the small screen: "Door unlocked." """
exit2 = """Finally, you press the button below, hopeful that you will escape this place. 
The doors open, revealing blinding light. You shield your eyes with your hand and step out."""
end1 = "You wake up."
end2 = "It was just a dream."
end3 = "With a smile on your face, you think it's time to submit your CV to Google."
