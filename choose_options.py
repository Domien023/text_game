import time, os
import dialogue

moves = []
equipment = []
class Items():
    def __init__(self, item):
        self.item = item
    def remove_item(self):
        equipment.remove(self.item)
    def add_item(self):
        while True:
            pick = input("\n\nPick up. *pick*\nBack. *back*\n: ")
            if pick == "pick":
                equipment.append(self.item)
                print(f"\n###{self.item} has been added to equipment###")
                time.sleep(1)
                break
            elif pick == "back":
                break

    def use_item(self):
        global use_or_back
        while True:
            option = input("\n\nUse item *use*\nBack *back*\n: ")
            if option == "use":
                print("\n")
                for item in equipment:
                    print(f"*{item}*")
                choose_item = input("What you want to use?\n: ")
                if choose_item.capitalize() == self.item:
                    os.system('cls')
                    use_or_back = 1
                    break
                else:
                    print("This item is useles here...")
            elif option == "back":
                use_or_back = 0
                break

def slowprint(text, sec):
    for letter in text:
        print(letter, end="")
        time.sleep(sec)
def awake():
    slowprint(dialogue.awake, 0.02)
    input("\nPress enter key to look around...")
    os.system('cls')
    slowprint(dialogue.room, 0.02)
def options1():
    print("\n\nWhat do you want to do?\n")
    print("""1. Examine computer. *examine*
2. Look around for any items. *look*
3. Proceed to the second room. *room2*""")
    if "Flashlight" in equipment:
        print("4. Inventory. *eq*")
def examine():
    if "power" not in moves:
        slowprint(dialogue.computer, 0.02)
    else:
        slowprint(dialogue.unlock_door, 0.02)
        moves.append("unlock_door")
def look():
    if "Flashlight" not in equipment:
        slowprint(dialogue.light, 0.02)
        Items("Flashlight").add_item()
    else:
        slowprint("Nothing...", 0.02)
def not_fl():
    slowprint(dialogue.dark_2room, 0.02)

def room2():
    if "Flashlight" in equipment and "room2" not in moves:
        slowprint(dialogue.dark_2room, 0.02)
        Items("Flashlight").use_item()
        if use_or_back == 1:
            os.system('cls')
            slowprint(dialogue.room2, 0.02)
            input("\nPress enter to continue...")
            os.system('cls')
            slowprint(dialogue.in_room2, 0.02)
            moves.append("room2")
    else:
        print("You move to the second room...")
        time.sleep(2)

def options2():
    print("\n\nWhat do you want to do?\n")
    print("""1. Proceed to investigate the table in the center. *table*
2. Examine the bed. *bed*
3. Check the contents of the wardrobe. *wardrobe*""")
    if "painting" not in moves:
        print("4. Inspect the slightly tilted painting on the wall. *painting*")
    else:
        print("4. Go to the fuse box. *box*")
    print("""5. Examine the metal doors with the peculiar screen. *door*
6. Back to first room. *back*
7. Equipment. *eq*""")
def table():
    slowprint(dialogue.table, 0.02)
def bed():
    slowprint(dialogue.bed, 0.02)
def wardrobe():
    if "Crowbar" not in equipment:
        slowprint(dialogue.wardrobe, 0.02)
        Items("Crowbar").add_item()
    else:
        print("Ordinary wardrobe.")
def painting():
    if "painting" not in moves:
        slowprint(dialogue.painting, 0.02)
        input("\nType *remove* to remove the painting... ")
        slowprint(dialogue.removed_painting, 0.02)
        moves.append("painting")
    elif "Fuse" not in equipment:
        print("Ordinary fuse box with a missing component.")
    else:
        slowprint(dialogue.box, 0.02)
        Items("Fuse").use_item()
        if use_or_back == 1:
            slowprint(dialogue.fuse, 0.02)
            Items("Fuse").remove_item()
            moves.append("power")
def door():
    if "unlock_door" not in moves:
        slowprint(dialogue.door, 0.02)
    else:
        slowprint(dialogue.exit1, 0.02)
        input("\nPress enter to open door...")
        slowprint(dialogue.exit2, 0.01)
        moves.append("exit")
def back():
    if "Crowbar" not in equipment or "Fuse" in equipment or "power" in moves:
        print("Back to first room...")
        time.sleep(2)
        moves.append("back")
    else:
        slowprint(dialogue.back, 0.02)
        Items("Crowbar").use_item()
        if use_or_back == 1:
            slowprint(dialogue.board, 0.02)
            Items("Fuse").add_item()
def eq():
    print("Your equipment:\n")
    for item in equipment:
        print(item)
def end():
    time.sleep(5)
    input("\nPress enter to continue...\n")
    slowprint(dialogue.end1, 0.04)
    time.sleep(1)
    print("\n")
    slowprint(dialogue.end2, 0.04)
    time.sleep(2)
    print("\n")
    slowprint(dialogue.end3, 0.04)
